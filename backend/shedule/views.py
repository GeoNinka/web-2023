from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie, vary_on_headers
from rest_framework.response import Response
from django.core.cache import cache

from rest_framework import viewsets
from shedule.models import Shedule
from shedule.serializers import SheduleSerializer

class SheduleViewSet(viewsets.ModelViewSet):
    serializer_class = SheduleSerializer
    def get_queryset(self):
        return Shedule.objects.all()
    
    @method_decorator(vary_on_cookie)
    @method_decorator(cache_page(60*60))
    def dispatch(self, *args, **kwargs):
        return super(SheduleViewSet, self).dispatch(*args, **kwargs)
