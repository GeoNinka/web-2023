from rest_framework import serializers
from userLogs.models import UserLogs
from django.contrib.auth.models import User

class UserLogsSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserLogs
        fields = ['id', 'user', 'path', 'visit_time']