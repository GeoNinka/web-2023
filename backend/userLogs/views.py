from rest_framework import viewsets
from userLogs.models import UserLogs
from userLogs.serializers import UserLogsSerializer

class UserLogsViewSet(viewsets.ModelViewSet):
    serializer_class = UserLogsSerializer
    def get_queryset(self):
        return UserLogs.objects.all()
