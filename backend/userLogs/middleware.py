from datetime import datetime
from .models import UserLogs

class UserLogsMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        
        if request.user.is_authenticated:
            UserLogs.objects.create(user=request.user,path=request.path, visit_time=datetime.now())

        return response