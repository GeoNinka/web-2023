from django.db import models
from django.contrib.auth.models import User

class UserLogs(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    path = models.CharField(max_length=255)
    visit_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return (str(self.user))

        