from django.contrib import admin
from .models import UserLogs

@admin.register(UserLogs)
class UserLogsAdmin(admin.ModelAdmin):
    list_display = ('user', 'path', 'visit_time')
    list_filter = ('user','path')
    search_fields = ("user",)