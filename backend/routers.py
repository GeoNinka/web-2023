from rest_framework import routers
from course.views import CourseViewSet
from mailingList.views import MailingListViewSet
from shedule.views import SheduleViewSet
from members.views import MembersViewSet
from back_api.views import UserViewSet
from userLogs.views import UserLogsViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet, basename='user')
router.register(r'course', CourseViewSet, basename='course')
router.register(r'mailingList', MailingListViewSet, basename='mailingList')
router.register(r'shedule', SheduleViewSet, basename='shedule')
router.register(r'members', MembersViewSet, basename='members')
router.register(r'userlogs', UserLogsViewSet, basename='userlogs')
