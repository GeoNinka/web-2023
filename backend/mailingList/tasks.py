from __future__ import absolute_import, unicode_literals

from celery import shared_task
from back_api.celery import app
from mailingList.models import MailingList 
from django.core.mail import send_mail

@app.task
def to_send_mail():
    list = MailingList.objects.all()
    emails = []
    for element in list:
        emails.append(element.email)
    send_mail("Подписка на рассылку", "Вы подписались на рассылку от нашего сермиса, будем спамить вам каждую минуту!", "root@localhost", emails)

@app.task
def send_mail_beat():
    return to_send_mail()
