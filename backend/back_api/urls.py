from django.contrib import admin
from django.urls import path, include
from routers import router
from course.views import CourseViewSet
from django.conf.urls.static import static
from django.conf import settings

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from drf_yasg import openapi
from drf_yasg.views import get_schema_view as swagger_get_schema_view

schema_view = swagger_get_schema_view(
    openapi.Info(
        title='API',
        default_version='1.0.0',
        description='API documentation',
    ),
    public=True,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/token/', TokenObtainPairView.as_view(),name='token_obtain_pair'),
    path('api-auth/', include('drf_social_oauth2.urls',namespace='drf')),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('api/', include((router.urls, 'courses'), namespace='courses')),
    path('api/', include((router.urls, 'mailingList'), namespace='mailingList')),
    path('api/', include((router.urls, 'members'), namespace='members')),
    path('api/', include((router.urls, 'userlogs'), namespace='userlogs')),
    path('swagger/schema/', schema_view.with_ui('swagger', cache_timeout=0), name='swagger-schema'),
    path('admin/clearcache/', include('clearcache.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)