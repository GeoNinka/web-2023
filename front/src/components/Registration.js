import axios from 'axios';
import React, { useState, useEffect, useRef } from "react";
import { baseURL } from "./../services/course.service";

const Registration = () => {
    const [login, setLogin] = useState()
    const [password, setPassword] = useState()

    return (
        <div className='login'>
            <form className='login-form'>
                <p>Регистрация</p>
                <input value={login} onChange={(e) => setLogin(e.target.value)} className=""  name="login" placeholder="Login"/>
                <input value={password} onChange={(e) => setPassword(e.target.value)} className="" type="password" name="password" placeholder="Password"/>
                <input className="" type="submit" value="Зарегистрироваться"/>
            </form>
        </div>
    )
}

export default Registration;