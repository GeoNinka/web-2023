import axios from 'axios';
import React, { useState, useEffect, useRef, useCallback } from "react";
import { baseURL } from "./../services/course.service";
import GoogleLogin from '@leecheuk/react-google-login';
import {gapi} from "gapi-script";

const Login = () => {
    const [login, setLogin] = useState()
    const [password, setPassword] = useState()
    const clientId = '260169985908-2ch6ipm9bh5enkkuoe30hlc8fba763j7.apps.googleusercontent.com';

    const success = async (text)=> {
        await localStorage.setItem("salesToken", text.access);
        window.location = "/";
    };
    
    const onSuccess = async (res) => {
        console.log('succzddgsfgess:', res.accessToken);
        const user = {
            "grant_type":"convert_token",
            "client_id":'l99mRA5WjJYegGA8adF4xPEz7nwY63AybdWmHPva',
            "client_secret": 'Itc1USAkk5AlfEQxQW6TGqnVbcet6g4pFXveYrvWgM5EeYc9mAubcRyLAqv9Sj62kyzZ0kADvQdPtPUnfxYJte1WPOyIHkgMOHJmkfckitDWlC6dS3hUHIuBvUgmxz7H',
            "backend":"google-oauth2",
            "token": res.accessToken
        };
        console.log(user)
        const {data} = await axios.post('http://localhost:8000/api-auth/convert-token/', user ,{headers: {
            'Content-Type': 'application/json'
        }}, {withCredentials: true});

        console.log(data, data['access_token'])
        axios.defaults.headers.common['Authorization'] = `Bearer ${data['access_token']}`;
        localStorage.clear();
        localStorage.setItem('access_token', data.access_token);
        localStorage.setItem('refresh_token', data.refresh_token);
        window.location.href = '/'
    }

    const onFailure = (err) => {
        console.log('failed:', err);
    };


    let handleSubmit = async (e) => {
        e.preventDefault();
        const response = await fetch(`${baseURL}/token/`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "username": login,
                    "password": password,
                })
            }
        );
        const text = await response.text();
        if (response.status === 200) {
            console.log("success", JSON.parse(text));
            success(JSON.parse(text))
        } else {
            console.log("failed", text);
            Object.entries(JSON.parse(text)).forEach(([key, value])=>{
            // fail(`${key}: ${value}`);
            });
        }
        setLogin('')
        setPassword('')
    }

    return (
        <div className='login'>
            <form className='login-form' onSubmit={handleSubmit}>
                <p>Войти в аккаунт</p>
                <input value={login} onChange={(e) => setLogin(e.target.value)} className=""  name="login" placeholder="Login"/>
                <input value={password} onChange={(e) => setPassword(e.target.value)} className="" type="password" name="password" placeholder="Password"/>
                <input className="" type="submit" value="Войти в аккаунт"/>
                <GoogleLogin
                    clientId={clientId}
                    buttonText="Sign in with Google"
                    onSuccess={onSuccess}
                    onFailure={onFailure}
                    cookiePolicy={'single_host_origin'}
                />
            </form>
        </div>
    )
}

export default Login;