import axios from 'axios';
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { baseURL } from "./../services/course.service";
import jwt_decode from 'jwt-decode'

const Header = () => {

    const[username, setUsername] = useState()

    const getUsername = async (id) => {
        try {
            const response = await axios.get(`${baseURL}/users/${id}`)
            setUsername(response.data.username)
        } catch (err) {
            console.error(err.toJSON())
        }
    }

    const logUserAction = async (e) => {
        if(localStorage.getItem("salesToken") != 'null' && localStorage.getItem("salesToken")) {
            axios.post(`${baseURL}/userlogs/`, {
                user: jwt_decode(localStorage.getItem("salesToken")).user_id,
                path: e.target.href
            }).then(response => {
                console.log(response)
            }).catch((e => {
                console.error(e)
            })) 
        }
    }

    useEffect(() => {
        if(localStorage.getItem("salesToken") != 'null' && localStorage.getItem("salesToken")) {
            let jwt = localStorage.getItem("salesToken")
            let decode = jwt_decode(jwt);
            getUsername(decode.user_id)
        } else {
            setUsername('Регистрация')
        }
    });
    const logout = async (e)=>{
        await localStorage.setItem("salesToken",null);
        await localStorage.setItem("access_token",null);
        await localStorage.setItem("refresh_token",null);
        window.location = "/login";
    };

    return (
        <div className="header" id="header">
            <div className="header__wrapper">
                <div className="header__navigation">
                    <ul className="header__links">
                        <li className="header__item"><Link exact to={"/"} className="header__link" onClick={logUserAction}>Главная</Link></li>                                                
                        <li className="header__item"><Link exact to={"/courses/"} className="header__link" onClick={logUserAction}>Тарифы</Link></li>  
                        <li className="header__item"><Link exact to={"/classes"} className="header__link" onClick={logUserAction}>Мастер-классы</Link></li>    
                        <li className="header__item"><Link exact to={"/programms"} className="header__link" onClick={logUserAction}>Программы</Link></li>                                                                                        
                        <li className="header__item"><a href="" className="header__link">Вопросы-ответы</a></li>
                    </ul>
                </div>
                <div className="header__right">
                    <form className="header__search">
                        <button className="header__search-button"><img src='/images/glass.svg' alt="Glass icon" className="header__search-icon"/></button>
                        <input className="header__search-input" id='search' placeholder="Поиск" type="text"/>
                        <label className="header__label" for='search'>Поиск</label>
                    </form>
                    <div className="header__login">
                        <Link exact to={"/login"} className="header__login-button"><img src="/images/login.svg" alt="Login icon" className="header__login-icon"/></Link>
                        <Link exact to={"/login"} className="header__login-link">{username}</Link>
                        <button className="header__login-button" onClick={logout}><img src='/images/logout.svg' alt="Login icon" className="header__login-icon"/></button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Header;